:icons: font

= [big red]*AWS Cognito with ReactJS*

Amazon Cognito is a platform that allows users create  authentication without worrying about the backend. This platform offers user pools and identity pools. User pools are user directories that provide sign-up and sign-in options for app users. Identity pools provide AWS credentials to grant users access to other AWS services.

image::images/aws-cognito.png[cognito logo, align="center"]

''''

=== [big green]*Getting Started*
Users will need to install *Node.js* and *NPM*. *Node.js* is a platform needed for the ReactJS apps. *NPM* is a package manager for JavaScript programming. To install *Node.js* and *NPM*, go to these websites.

* Click here for https://nodejs.org/en/download/[*Node.js*]

* Click here for https://www.npmjs.com/get-npm[*NPM*]

Next, *React JS* needs to be installed globally. Use the following command:

* `npm install -g create-react-app`

Visit this website https://reactjs.org/blog/2016/07/22/create-apps-with-no-configuration.html[*React JS*] for more information on *React JS*.

''''

=== [big green]*Creating A New App*

Now  it is now time to begin developing your new app.

* Run this command: `npx create-react-app  <name of app>` 

** This will create the project downloading the template and installing dependencies.
+
--
image::images/starting-app.png[react js start 500, 500, align="center"]
--
* CD into the project

* Run this command: ``npm i -g @aws-amplify/cli``

- This adds the Amplify CLI which enables the developer to create user and identity pools via command line.
+
--
image::images/awscli-success.png[aws cli success image]

* Run this command: `amplify configure`

- You should be signed into your AWS account. This will take you through a series of steps to set up a username, a new admin user, and generate a secret access key and access key id which will be needed later. Take note for safe keeping.
+
--
image::images/aws-config1.png[aws config step 1 500, 500, align="center"]
image::images/aws-config2.png[aws config step 2 500, 500, align="center"]
image::images/aws-config3.png[aws config step 3 500, 500, align="center"]
image::images/aws-config4.png[aws config step 4 500, 500, align="center"]
image::images/aws-config5.png[aws config step 5 500, 500, align="center"]

* Return to the terminal and hit `Enter`. You will be prompted to add the `Access key ID` and `Secret access key` from the *IAM Manager* on *AWS*.
+
--
image::images/amplify-config6.png[aws config step 6 900,align="center"]



* Run this command: `` amplify init ``

** This will run you through a step of options to choose the setting that suit the project best. 
+
--
image::images/amplify-init1.png[amplify init1, 900 ,align="center"]
image::images/amplify-init2.png[amplify init2, 900 ,align="center"]
--

* After deployment is complete, a generated file named [red]`aws-exports.js` will appear in the [red]`src` folder. [bold]*Do not modify this file.*

* Now, we need to add the authentication resource to the app. 

* Run this command: `amplify add auth`

- You can chose either `Default configuration` or `Manual configuration`. `Manual configuration` allows you to have the best configuration options.
+
--
image::images/aws-auth-1.png[aws add auth, 700,align="center"]
--

--
image::images/amplify-add-auth.png[amplify add auth, 700 ,align="center"]
--


* In order to deploy the new resource changes to the cloud, run: `amplify push`. This may take a few minutes.
+
--
image::images/amplify-push.png[amplify push, 700, align="center"]
image::images/amplify-push2.png[amplify push, 700, align="center"]
--

* Now we need to install dependencies into the app. Run this command: `yarn add aws-amplify`.

* If you now look at the [red]`aws-exports.js` file, it has been populated with new information. 

* Next, import the following into App.js:

- `import *{ Auth }* from 'aws-amplify';`


* The Amplify and Cognito setup is complete. Now.... [bold red]*CODE!*

