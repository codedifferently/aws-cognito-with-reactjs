:icons: font

= [red]*Creating Federated Identity*


Amazon Federated Identities are for authorization (access control). Identity pools create unique identities for users and give them access to other AWS services.

image::https://miro.medium.com/max/1504/1*IJ_32EsQUZxzw4GJhRY3GQ.png[user pool]

* Click on [blue]*Federated Identities* and then select [blue]*Create new identity pool*. Enter the name of the app and then under *Authentication flow settings*, [red]*check* the option `Allow Basic (Classic) Flow`. Next, under *Authentication Providers* and in the *Cognito* tab, enter the `User Pool ID` and the `App client Id` values. 
+
--
image::images/create-idpool.png[create identity pool]
--
Click, [blue]*Create Pool*.

* Identify the Identity and Access Management(IAM). IAM is used to control who is signed in and has permission to use resources.
+
--
image::images/iam.png[IAM, 400, align="center"]
--
+
NOTE: Defaults are already selected. Click [blue]*Allow* to continue.

* The next screen displays the sample code under *Get AWS Credentials*. In the sample code the values for the `Identity pool ID` and the `Region` are displayed. Take note of both of them. 
+
--
image::images/id-pool-sample-code.png[id pool sample code, 600, align="center"]